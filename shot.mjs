import puppeteer from "puppeteer-core";
import chromium from "@sparticuz/chromium";
import Jimp from "jimp";

export async function shot (event){
	const browser = await puppeteer.launch({
		args: chromium.args,
		defaultViewport: chromium.defaultViewport,
		executablePath: await chromium.executablePath(),
		headless: chromium.headless,
		ignoreHTTPSErrors: true,
	});

	const page = await browser.newPage();
	console.log( 'https://'+event.pathParameters.url )
	await page.goto( 'https://'+event.pathParameters.url );
	const pageTitle = await page.title();

	page.setViewport({
		width: 1600,
		height: 900
	})

	console.log(pageTitle);

	// TODO should be POST params
	await page.click('#onetrust-accept-btn-handler')
	await page.waitForTimeout(500)

	await page.click('[aria-controls="artists_range"]')
	await page.click('[data-partial-href="/user/TomChiverton/partial/artists?artists_date_preset=LAST_30_DAYS"]')

	var p = {fullPage:true};

	var buff = await page.screenshot( p );

	if( event.queryStringParameters && event.queryStringParameters.x ){
		console.log(event.queryStringParameters)
		buff = await Jimp.read(buff)
		buff.crop(
			Number(event.queryStringParameters.x),
			Number(event.queryStringParameters.y),

			Number(event.queryStringParameters.width),
			Number(event.queryStringParameters.height)
		)
		buff = await buff.getBufferAsync( Jimp.MIME_PNG )
	}

	await browser.close();

	//TODO clip x,y w,h as query param

	return {
	'headers': { "Content-Type": "image/png" },
            'statusCode': 200,
            'body': buff.toString('base64'),
            'isBase64Encoded': true
          }
};