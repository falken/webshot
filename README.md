# WebShot

A simple API to screen shot a (part of) web page

## Usage

The layer comes from https://www.npmjs.com/package/@sparticuz/chromium

### Deployment

```
$ serverless deploy
```

After deploying, you should see output similar to:

```bash
Deploying webpage-shot to stage dev (eu-west-2)

✔ Service deployed to stack webpage-shot-dev (52s)
```

_Note_: In current form, after deployment, your API is public and can be invoked by anyone. For production deployments, you might want to configure an authorizer. For details on how to do that, refer to [http event docs](https://www.serverless.com/framework/docs/providers/aws/events/apigateway/).

### Invocation

```curl https://xxxxxxx.execute-api.eu-west-2.amazonaws.com/shot/{hostname}/{path}```
ex
```curl https://xxxxxxx.execute-api.eu-west-2.amazonaws.com/shot/last.fm/user/tomchiverton```

HTTPS prefix is assumed, and required.

#### Optional cropping

```curl https://xxxxxxx.execute-api.eu-west-2.amazonaws.com/shot/last.fm/user/tomchiverton?x=215&y=1084&width=770&height=384```

### Test
After successful deployment, you can call the created application via HTTP:

```bash
curl https://xxxxxxx.execute-api.us-east-1.amazonaws.com/
```

Which should result in response similar to the following (removed `input` content for brevity):

```json
{
  "message": "Go Serverless v2.0! Your function executed successfully!",
  "input": {
    ...
  }
}
```